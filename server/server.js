const fs = require('fs');
const os = require('os');
const split = require('split');
const through = require('through2');

const LOG_FILE = '../tmp/hw.log';
const src = fs.createReadStream(LOG_FILE);

src.pipe(split(os.EOL))
.pipe(through(
  function (chunk, _, next) {
    if (chunk.includes('JavaScript')) {
      this.push(chunk + '\n');
    }
    next();
  })
)
.pipe(fs.createWriteStream('../tmp/filtered.log'));
